import { NgModule } from '@angular/core';
import { EncomendasComponent } from './encomendas.component';
import { Routes, RouterModule } from '@angular/router';
import {OrdersComponent} from './orders/orders.component';

const routes: Routes = [
  {
    path: '',
    component: EncomendasComponent
  },
  {
    path: 'orders',
    component: OrdersComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EncomentasRoutingModule {}
