import { EncomentasRoutingModule } from './encomendas-routing.module';
import { EncomendasComponent } from './encomendas.component';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { OrdersService } from '../shared/services/orders.service';
import {OrdersComponent} from './orders/orders.component';
import { ConfirmationService, MessageService } from 'primeng/api';
import {ScrollPanelModule} from 'primeng/scrollpanel';


@NgModule({
  imports: [
    SharedModule,
    EncomentasRoutingModule,
    ScrollPanelModule
  ],
  exports: [],
  providers: [
    OrdersService,
    ConfirmationService,
    MessageService
    ],
  declarations: [
    EncomendasComponent,
    OrdersComponent
  ]
})
export class EncomendasModule {}
