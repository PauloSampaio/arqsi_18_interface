import {Component, Inject, Input, OnInit, Output} from '@angular/core';
import { OrdersService } from '../shared/services/orders.service';
import {Catalog} from '../shared/models/catalog.model';
import {Product} from '../shared/models/product.model';
import {Router} from '@angular/router';
import {FormControl, FormGroup} from '@angular/forms';
import {Material} from '../shared/models/material.model';
import {element} from 'protractor';
import {Category} from '../shared/models/category.model';
import {Finish} from '../shared/models/finish.model';
import {Dimensions} from '../shared/models/dimensions.model';
import {Item} from '../shared/models/item.model';
import {Order} from '../shared/models/order.model';
import {ConfirmationService, Message, MessageService} from 'primeng/api';



@Component({
  selector: 'app-encomendas',
  templateUrl: './encomendas.component.html',
  styleUrls: ['./encomendas.component.css']
})
export class EncomendasComponent implements OnInit {

  public key = 0;

  public order: Order;
  public items: Item[];

  public msgs: Message[] = [];

  public catalogs: Catalog[];
  public catalog: Catalog;
  public products: Product[];
  public product: Product;
  public productFromItem: Product;
  public partsProduct = new Array<Product>();

  public allMaterials: Material[];
  public materialsToDisplay = new Array<Material>();
  public material: Material;

  public allProducts: Product[];
  public productsToDisplay = new Array<Product>();
  public product_part: Product;

  public allCategories: Category[];
  public categoryToDisplay: Category;
  public categoryToDisplayPart: Category;

  public allFinishes: Finish[];
  public finishToDisplay: Finish[];
  public finish: Finish;

  public allDimensions: Dimensions[];
  public dimensionsToDisplay = new Array<Dimensions>();

  public selectedPart: number[];
  public selectedMaterial: number;
  public selectedFinish: number;

  public length: number;
  public width: number;
  public height: number;

  public display = false;
  public display_mat = false;
  public display_prod = false;
  public prod_to_display = false;
  public no_prod_to_display = false;
  public showOrder;
  public header: string;
  public header_mat: string;
  public header_prod: string;

  constructor(
    private ordersService: OrdersService,
    private router: Router,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) { }

  ngOnInit() {
    this.getCatalogs();
    this.getMaterials();
    this.getProducts();
    this.getCategories();
    this.getFinishes();
    this.getDimensions();
    this.items = new Array<Item>();
  }

  public getCatalogs(): void {
    this.ordersService.getCatalogs().subscribe((data: any) => {
      this.catalogs = data;
    });
  }

  public getProductsCatalog(cat: Catalog): void {
    this.ordersService.getProductsCatalog(cat.id).subscribe((data: any) => {
      this.products = data;
    });
  }

  goSearch(cat: Catalog) {
    this.catalog = cat;
    this.router.navigate(['/orders']);
  }

  public selectProduct(prod: Product, num: number): void {
    this.header = prod.name;
    this.display = true;


    this.finishToDisplay = new Array<Finish>();
    // if it is a new product
    if (num === 0) {
      this.partsProduct = new Array<Product>();
      this.product = prod;

    this.getMaterialsProduct(this.product.materials);
    this.getPartsProduct(this.product.partsIds);
    this.selectCategory(this.product.categoryId);
    this.getDimensionsProduct(this.product.dimensionses);
    this.selectedFinish = null;
    this.selectedMaterial = null;
    this.selectedPart = null;
    this.length = null;
    this.width = null;
    this.height = null;
    this.productFromItem = null;

    } else {

      // if it his an edition of a part product
      this.getMaterialsProduct(this.productFromItem.materials);
      this.getPartsProduct(this.productFromItem.partsIds);
      this.selectCategory(this.productFromItem.categoryId);
      this.getDimensionsProduct(this.productFromItem.dimensionses);

      this.selectedFinish = null;
      this.selectedMaterial = null;
      this.selectedPart = null;
      this.length = null;
      this.width = null;
      this.height = null;
    }
  }

  public getMaterials(): void {
     this.ordersService.getMaterials().subscribe((data: any) => {
      this.allMaterials = data;
    });
  }

  public getMaterialsProduct(materials: number[]): void {

    this.materialsToDisplay = new Array<Material>();

    this.allMaterials.forEach( mat => {
      if (materials.includes(mat.id)) {
       this.materialsToDisplay.push(mat);
      }
    });
  }

  public selectMaterial(mat: Material): void {
    this.header_mat = mat.name;
    this.display_mat = true;
    this.finishToDisplay = new Array<Finish>();
    this.material = mat;
    this.getFinishesMaterial(this.material.finishes);
  }

  public getProducts(): void {
    this.ordersService.getProducts().subscribe((data: any) => {
      this.allProducts = data;
    });
  }

  public getPartsProduct(parts: number[]): void {

    this.productsToDisplay = new Array<Product>();

    this.allProducts.forEach( prod => {
      if (parts.includes(prod.productId)) {
        this.productsToDisplay.push(prod);
      }
    });

    if (this.productsToDisplay.length > 0) {
      this.prod_to_display = true;
      this.no_prod_to_display = false;
    } else {
      this.prod_to_display = false;
      this.no_prod_to_display = true;
    }
  }

  public selectProductPart(prod: Product): void {
    this.header_prod = prod.name;
    this.display_prod = true;
    this.product_part = prod;
    this.selectCategoryPart(this.product_part.categoryId);
  }

  public getCategories() {
    this.ordersService.getCategories().subscribe((data: any) => {
      this.allCategories = data;
    });
  }

  public selectCategory(category: number): string {

    this.categoryToDisplay = new Category();

    this.allCategories.forEach( cat => {
      if (cat.id === category) {
        this.categoryToDisplay = cat;
        return;
      }
    });

    return this.categoryToDisplay.name;
  }

  public selectCategoryPart(category: number): void {

    this.categoryToDisplayPart = new Category();

    this.allCategories.forEach( cat => {
      if (cat.id === category) {
        this.categoryToDisplayPart = cat;
        return;
      }
    });
  }

  public getFinishes() {
    this.ordersService.getFinishes().subscribe((data: any) => {
      this.allFinishes = data;
    });
  }

  public getFinishesMaterial(finishes: number[]) {

    this.finishToDisplay = new Array<Finish>();

    this.allFinishes.forEach(fin => {
      if (finishes.includes(fin.id)) {
        this.finishToDisplay.push(fin);
      }
    });
  }

  public getDimensions(): void {
    this.ordersService.getDimensions().subscribe((data: any) => {
      this.allDimensions = data;
    });
  }

  public getDimensionsProduct(dimension: number[]): void {

    this.dimensionsToDisplay = new Array<Dimensions>();

    this.allDimensions.forEach(dim => {
      if (dimension.includes(dim.id)) {
        this.dimensionsToDisplay.push(dim);
      }
    });
  }

  public addItem(product: Product): void {

    this.showOrder = true;

    // PARA TRATAMENTO DO PRODUCTO BASE
    if (this.productFromItem === null) {
      const item = new Item(
        null,
        product.productId,
        parseInt(this.selectedMaterial.toString(), 10),
        parseInt(this.selectedFinish.toString(), 10),
        this.length,
        this.width,
        this.height,
        0,
        null
      );
      this.items.push(item);

      this.partsProduct.forEach(part => {
        const item_part = new Item(
          null,
          part.productId,
          null,
          null,
          0,
          0,
          0,
          product.productId,
          null
        );
        this.items.push(item_part);
      });

    } else {
      // PARA TRATAMENTO DA EDIÇÃO DO PRODUTO FILHO;

      this.items.forEach((element3, index) => {

        if (index === this.key) {

          const item_part = new Item(
            null,
            element3.ProductId,
            parseInt(this.selectedMaterial.toString(), 10),
            parseInt(this.selectedFinish.toString(), 10),
            this.length,
            this.width,
            this.height,
            element3.ParentId,
            null
          );
          this.items.splice(index, 1, item_part);
          /*
          if (this.selectedMaterial !== null) {
            element3.MaterialId = this.selectedMaterial;
          }
          if (this.selectedFinish !== null) {
            element3.FinishId = this.selectedFinish;
          }

          element3.Length = this.length;
          element3.Width = this.width;
          element3.Height = this.height;
          */
        }
      });
    }

    this.display = false;
  }

  public addPart(product: Product): void {
    this.partsProduct.push(product);
  }

  public removePart(): void {
    this.partsProduct.pop();
  }

  public saveOrder(items: Item[]): void {
    this.order = new Order();
    this.order.items = items;

    let dataMessage = null;

    this.confirmationService.confirm({
      message: 'Are you sure that you want to Save this Order?',
      header: 'Save Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.ordersService.newOrder(this.order).subscribe((data: any) => {

          dataMessage = data;

          // Encaminha para Página Listar Encomendas
          this.router.navigate(['/orders']);
        }, error => {
          console.log('ERROR: ', error.error);
          this.msgs = [{ severity: 'error', summary: 'Rejected', detail: 'There was an error!' }];
        });
        // this.msgs = [{ severity: 'success', summary: 'Confirmed', detail: 'You have accepted - ' + dataMessage }];
      },
      reject: () => {
        this.msgs = [{ severity: 'error', summary: 'Rejected', detail: 'You have rejected' }];
      }
    });
  }

  public removeItemFromPreOrder(item: Item): void {
    this.items.forEach((element2, index) => {
      if (item === element2) {
        this.items.splice(index, 1);
      }
    });
    if (this.items.length === 0) {
      this.showOrder = false;
    }
  }

  public editItemFromPreOrder(item: Item, key: number): void {

    this.allProducts.forEach(prod => {

      if (item.ProductId === prod.productId) {

        this.productFromItem = prod;
        this.key = key;
      }
    });
    this.selectProduct(this.productFromItem, 1);
  }

  // Receives the Selected Concrete User Dimensions in parameter, and checks if they fit in the dimensions of a Dimensions object
  public checkSelectedDimensions(length: number, width: number, height: number, dimensionsToFitInto: Dimensions): boolean {

    const selectedDimensions = new Dimensions(length, length, width, width, height, height);

    return selectedDimensions.checkFitsIn(dimensionsToFitInto);
  }

  public getProductName(productId: number): string {
    let name = 'N/A';
    this.allProducts.forEach(prod => {
      if (prod.productId === productId) {
        name = prod.name;
      }
    });
    return name;
  }

  public getMaterialName(materialId: number): string {
    let name = 'N/A';
    this.allMaterials.forEach(mat => {
      if (mat.id === materialId) {
        name = mat.name;
      }
    });
    return name;
  }

  public getFinishName(finishId: number): string {
    let name = 'N/A';
    this.allFinishes.forEach(fin => {
      if (fin.id === finishId) {
        name = fin.name;
      }
    });
    return name;
  }

  public onBeforeDialogHide(): void {
    this.display = false;
  }
}
