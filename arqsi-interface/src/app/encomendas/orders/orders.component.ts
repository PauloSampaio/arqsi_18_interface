import {Component, Input, OnInit, ɵdisableBindings} from '@angular/core';
import {OrdersService} from '../../shared/services/orders.service';
import {Product} from '../../shared/models/product.model';
import {Catalog} from '../../shared/models/catalog.model';
import {Order} from '../../shared/models/order.model';
import {ConfirmationService, Message, MessageService} from 'primeng/api';
import {Item} from '../../shared/models/item.model';
import {Router} from '@angular/router';
import {Finish} from '../../shared/models/finish.model';
import {Material} from '../../shared/models/material.model';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  msgs: Message[] = [];
  public display = false;
  public header: string;

  public orders: Order[];
  public allItems: Item[];
  public orderItems: Item[];
  public order: Order;

  public allProducts: Product[];
  public allMaterials: Material[];
  public allFinishes: Finish[];

  constructor(
    private orderService: OrdersService,
    private confirmationService: ConfirmationService,
    private router: Router,
    private messageService: MessageService
  ) { }

  ngOnInit() {
    this.getOrders();
    this.getItems();
    this.getProducts();
    this.getMaterials();
    this.getFinishes();
  }

  public navigateToCreatOrderPage() {
    this.router.navigate(['/encomendas']);
  }

  public getOrders(): void {
    this.orderService.getOrders().subscribe((data: any) => {
      this.orders = data;
    });
  }

  public getItems(): void {
    this.orderService.getItems().subscribe((data: any) => {
      this.allItems = data;
    });
  }

  public getProducts(): void {
    this.orderService.getProducts().subscribe((data: any) => {
      this.allProducts = data;
    });
  }

  public getProductName(productId: number): string {
    let name = 'N/A';
    this.allProducts.forEach(prod => {
      if (prod.productId === productId) {
        name = prod.name;
      }
    });
    return name;
  }

  public getMaterials(): void {
    this.orderService.getMaterials().subscribe((data: any) => {
      this.allMaterials = data;
    });
  }

  public getMaterialName(materialId: number): string {
    let name = 'N/A';
    this.allMaterials.forEach(mat => {
      if (mat.id === materialId) {
        name = mat.name;
      }
    });
    return name;
  }

  public getFinishes() {
    this.orderService.getFinishes().subscribe((data: any) => {
      this.allFinishes = data;
    });
  }

  public getFinishName(finishId: number): string {
    let name = 'N/A';
    this.allFinishes.forEach(fin => {
      if (fin.id === finishId) {
        name = fin.name;
      }
    });
    return name;
  }

  public selectItemsOrder(order: Order): void {
    this.orderItems = new Array<Item>();
    this.order = order;
    this.allItems.forEach(item => {
      if (item.order === order._id) {
        this.orderItems.push(item);
      }
    });

    if (this.orderItems.length === 0) {
      this.msgs = [{  detail: 'The order is empty!' }];
    } else {
      this.header = 'Order N.º ' + order._id;
      this.display = true;
    }
  }

  public deleteOrder(order: Order): void {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to Delete this Order?',
      header: 'Delete Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.orderService.deleteOrder(order).subscribe((data: any) => {
          console.log('orderService', data);
          this.getOrders();
          this.msgs = [{ severity: 'success', summary: 'Confirmed', detail: 'Order Deleted' }];
        });
      },
      reject: () => {
        this.msgs = [{ severity: 'error', summary: 'Rejected', detail: 'You have rejected' }];
      }
    });
  }

  public deleteItem(item: Item): void {

    this.display = false;

    this.confirmationService.confirm({
      message: 'Are you sure that you want to Delete this Item?',
      header: 'Delete Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.orderService.deleteItem(item).subscribe((data: any) => {

          this.msgs.push({ severity: 'success', summary: 'Success Message', detail: data });

          this.getItems();

          let index = 0;
          while (index < this.orderItems.length) {
            if (this.orderItems[index].ParentId === item.ProductId || this.orderItems[index].ProductId === item.ProductId) {
              this.orderItems.splice(index, 1);
              index = 0;
            } else {
              index++;
            }
          }
        });

        if (this.orderItems.length === 0) {
          this.display = false;
          this.msgs = [{  detail: 'The order is empty!' }];
        }
      },
      reject: () => {
        this.display = true;
      }
    });
  }

  clearMessages() {
    this.messageService.clear();
  }

}
