import { Component, OnInit } from '@angular/core';
import {Catalog} from '../../shared/models/catalog.model';
import {Product} from '../../shared/models/product.model';
import {ConfirmationService, Message, MessageService} from 'primeng/api';
import {FormControl, FormGroup} from '@angular/forms';
import {CatalogService} from '../../shared/services/catalog.service';
import {ProductService} from '../../shared/services/product.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css']
})
export class CatalogComponent implements OnInit {
  public catalogs: Catalog[];
  public catalog: Catalog;
  public products = new Array<Product>();
  public productsArray: Product[];
  public productsToSave = new Array<number>();
  public display = false;
  public header: string;
  msgs: Message[] = [];

  public dateAux: any;

  public catalogForm = new FormGroup({
    id: new FormControl(''),
    name: new FormControl(''),
    description: new FormControl(''),
    date: new FormControl(''),
    products: new FormControl()
  });


  constructor(
    public datepipe: DatePipe,
    private catalogService: CatalogService,
    private productService: ProductService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) { }

  ngOnInit() {
    this.getCatalogs();
    this.getProducts();
  }

  public getCatalogs(): void {
    this.catalogService.getCatalogs().subscribe((data: any) => {
      this.catalogs = data;
    });
  }

  public getProducts(): void {
    this.productService.getProduct().subscribe((data: any) => {
      this.products = data;
    });
  }

  // get products by id
  public getProductsCatalog(cproducts: number[]): string[] {
    const productsToDisplay = new Array<string>();
    // this.finishesToDisplay = new Array<Finish>();
    this.products.forEach( prod => {
      if (cproducts.includes(prod.productId)) {
        productsToDisplay.push(prod.name + ' ');
      }
    });
    return productsToDisplay;
  }

  public newCatalog(): void {
    this.header = 'New Catalog';
    this.display = true;
    this.catalogForm.get('name').setValue('');
    this.catalogForm.get('description').setValue('');
    this.catalogForm.get('date').setValue('');
    this.catalogForm.get('products').setValue(null);
  }

  public editCatalog(cat: Catalog): void {
    // Atribuicao para validacao da data no save
    this.dateAux = cat.date;

    this.header = 'Edit Catalog';
    this.display = true;
    this.catalogForm.get('id').setValue(cat.id);
    this.catalogForm.get('name').setValue(cat.name);
    this.catalogForm.get('description').setValue(cat.description);
    this.catalogForm.get('date').setValue(cat.date);
    this.catalogForm.get('products').setValue(cat.products);
  }

  public saveCatalog(): void {

    this.productsToSave = new Array<number>();
    this.productsArray = this.catalogForm.get('products').value;


    if (this.productsArray != null) {
      this.productsArray.forEach(element3 => {
        if (element3.productId != null) {
          this.productsToSave.push(element3.productId);
        }
      });
    }
    this.catalog = new Catalog(
      this.catalogForm.get('id').value,
      this.catalogForm.get('name').value,
      this.catalogForm.get('description').value,
      this.catalogForm.get('date').value,
      this.productsToSave
    );

    // FORMATAçÂo da Data
    if (this.dateAux !== this.catalogForm.get('date').value) {
      const tempdate = this.datepipe.transform(this.catalog.date, 'MM/dd/yyyy');
      this.catalog.date = tempdate;
    } else {
      const array = this.catalogForm.get('date').value.split('/');
      this.catalog.date = array[1].concat('-', array[0], '-', array[2]);
    }

    if (this.header === 'New Catalog') { this.catalogService.newCatalog(this.catalog).subscribe((data: any) => {
      this.getCatalogs();
      this.showSuccessMessage();
      this.display = false;
    });
    }
    if (this.header === 'Edit Catalog') { this.catalogService.updateCatalog(this.catalog).subscribe((data: any) => {
      this.getCatalogs();
      this.showSuccessMessage();
      this.display = false;
    });
    }
  }

  public deleteCatalog(cat: Catalog): void {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to Delete this Catalog?',
      header: 'Delete Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.catalogService.deleteCatalog(cat).subscribe((data: any) => {
          this.getCatalogs();
        });
        this.msgs = [{ severity: 'success', summary: 'Confirmed', detail: 'You have accepted' }];
      },
      reject: () => {
        this.msgs = [{ severity: 'error', summary: 'Rejected', detail: 'You have rejected' }];
      }
    });

  }

  public showSuccessMessage(): void {
    this.msgs = [];
    this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Catalog Saved' });
  }

  clearMessages() {
    this.messageService.clear();
  }

  public onBeforeDialogHide(): void {
    this.display = false;
  }
}
