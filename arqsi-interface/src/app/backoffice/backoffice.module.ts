import { ConfirmationService, MessageService } from 'primeng/api';
import { FinishesService } from '../shared/services/finishes.service';
import { DimensionsService } from '../shared/services/dimensions.service';
import { MaterialService } from '../shared/services/material.service';
import { ProductService} from '../shared/services/product.service';
import { CatalogService} from '../shared/services/catalog.service';
import { CategoryService} from '../shared/services/category.service';
import { BackofficeRoutingModule } from './backoffice.routing.module';
import { BackofficeComponent } from './backoffice.component';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { MaterialComponent } from './material/materialList/material.component';
import { ProductComponent} from './product/product.component';
import { FinishComponent} from './finish/finish.component';
import { CatalogComponent} from './catalog/catalog.component';
import { CategoryComponent} from './category/category.component';
import {DropdownModule} from 'primeng/dropdown';
import {CalendarModule} from 'primeng/calendar';
import {SliderModule} from 'primeng/slider';

@NgModule({
  imports: [
    SharedModule,
    BackofficeRoutingModule,
    DropdownModule,
    CalendarModule,
    SliderModule
  ],
  exports: [],
  providers: [
    CategoryService,
    CatalogService,
    ProductService,
    MaterialService,
    DimensionsService,
    FinishesService,
    ConfirmationService,
    MessageService
  ],
  declarations: [
    BackofficeComponent,
    MaterialComponent,
    ProductComponent,
    FinishComponent,
    CatalogComponent,
    CategoryComponent
  ]
})
export class BackofficeModule {}
