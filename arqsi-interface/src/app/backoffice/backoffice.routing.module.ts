import { BackofficeComponent } from './backoffice.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaterialComponent } from './material/materialList/material.component';
import { ProductComponent} from './product/product.component';
import {FinishComponent} from './finish/finish.component';
import {CategoryComponent} from './category/category.component';
import {CatalogComponent} from './catalog/catalog.component';

const routes: Routes = [
  {
    path: '',
    component: BackofficeComponent
  },
  {
    path: 'product',
    component: ProductComponent
  },
  {
    path: 'material',
    component: MaterialComponent
  },
  {
    path: 'finish',
    component: FinishComponent
  },
  {
    path: 'category',
    component: CategoryComponent
  },
  {
    path: 'catalog',
    component: CatalogComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BackofficeRoutingModule {}
