import { Component, OnInit } from '@angular/core';
import {FinishesService} from '../../shared/services/finishes.service';
import {Finish} from '../../shared/models/finish.model';
import {FormControl, FormGroup} from '@angular/forms';
import {ConfirmationService, Message, MessageService} from 'primeng/api';

@Component({
  selector: 'app-finish',
  templateUrl: './finish.component.html',
  styleUrls: ['./finish.component.css']
})
export class FinishComponent implements OnInit {

  public finishes: Finish[];

  public finish: Finish;

  public finishForm = new FormGroup({
    id: new FormControl(''),
    name: new FormControl(''),
    description: new FormControl('')
  });

  msgs: Message[] = [];
  public header: string;
  public display = false;

  constructor(
    private finishService: FinishesService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) { }

  ngOnInit() {
    this.getFinishes();
  }

  public getFinishes(): void {
    this.finishService.getFinishes().subscribe((data: any) => {
      this.finishes = data;
    });
  }

  public newFinish(): void {
    this.header = 'New Finish';
    this.display = true;
    this.finishForm.get('name').setValue('');
    this.finishForm.get('description').setValue('');
  }

  public editFinish(finish: Finish): void {
    this.header = 'Edit Finish';
    this.display = true;
    this.finishForm.get('id').setValue(finish.id);
    this.finishForm.get('name').setValue(finish.name);
    this.finishForm.get('description').setValue(finish.description);
  }

  public saveFinish(): void {
    this.finish = new Finish(
      this.finishForm.get('id').value,
      this.finishForm.get('name').value,
      this.finishForm.get('description').value
    );
    if (this.header === 'New Finish') { this.finishService.newFinishes(this.finish).subscribe((data: any) => {
      this.getFinishes();
      this.showSuccessMessage();
      this.display = false;
    });
    }

    if (this.header === 'Edit Finish') { this.finishService.updateFinish(this.finish).subscribe((data: any) => {
      this.getFinishes();
      this.showSuccessMessage();
      this.display = false;
    });
    }
  }

  public deleteFinish(finish: Finish): void {
    this.confirmationService.confirm({
      message: 'Are You sure that you want to Delete this Finish?',
      header: 'Delete Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.finishService.deleteFinish(finish).subscribe((data: any) => {
          this.getFinishes();
        });
        this.msgs = [{severity: 'success', summary: 'Confirmed', detail: 'You have accepted'}];
      },
      reject: () => {
        this.msgs = [{severity: 'error', summary: 'Rejected', detail: 'You have rejected'}];
      }
    });
  }

  public showSuccessMessage(): void {
    this.msgs = [];
    this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Finish Saved' });
  }

  clearMessages() {
    this.messageService.clear();
  }

  public onBeforeDialogHide(): void {
    this.display = false;
  }
}
