import { Category } from '../../shared/models/category.model';
import { CategoryService } from '../../shared/services/category.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Message, ConfirmationService, MessageService } from 'primeng/api';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  public categories = new Array<Category>();

  public category: Category;

  public parents = new Array<Category>();

  public parent: Category;

  public display = false;
  public header: string;

  msgs: Message[] = [];

  public categoryForm = new FormGroup({
    id: new FormControl(''),
    name: new FormControl(''),
    description: new FormControl(''),
    parentId: new FormControl(),
    children: new FormControl()
  });

  constructor(
    private categoryService: CategoryService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) { }

  ngOnInit() {
    this.getCategories();
  }

  showDialog() {
    this.display = true;
  }

  public getCategories(): void {
    this.categoryService.getCategory().subscribe((data: any) => {
      this.categories = data;
      this.parents = data;
      // console.log(data);
    });
  }

  // get children categories by id
  public getChildrenById(mcategories: number[]): string[] {
    const categoriesToDisplay = new Array<string>();
    this.categories.forEach( cat => {
      if (mcategories.includes(cat.id)) {
        categoriesToDisplay.push(cat.name + ' ');
      }
    });
    return categoriesToDisplay;
  }

  // get parent categories by id
  public getParentsById(cparent: number): string {
    let parentToDisplay = 'No Parent';
    this.parents.forEach((parent) => {
      if (parent.id === cparent) {
        parentToDisplay = parent.name;
      }
    });
    return parentToDisplay;
  }

  public newCategory(): void {
    this.header = 'New Category';
    this.display = true;
    this.categoryForm.get('name').setValue('');
    this.categoryForm.get('description').setValue('');
    this.categoryForm.get('parentId').setValue('');
  }

  public editCategory(cat: Category): void {
    this.header = 'Edit Category';
    this.display = true;
    this.categoryForm.get('id').setValue(cat.id);
    this.categoryForm.get('name').setValue(cat.name);
    this.categoryForm.get('description').setValue(cat.description);
    this.categoryForm.get('parentId').setValue(cat.parentId);
  }

  public saveCategory(): void {

    this.parent = this.categoryForm.get('parentId').value;

    this.category = new Category(
      this.categoryForm.get('id').value,
      this.categoryForm.get('name').value,
      this.categoryForm.get('description').value,
      this.parent.id,
      null
    );

      if (this.header === 'New Category') {
        this.categoryService.newCategory(this.category).subscribe((data: any) => {
          this.getCategories();
          this.showSuccessMessage();
          this.display = false;
        });
      }

      if (this.header === 'Edit Category') {

        if (this.category.id === this.category.parentId) {
          this.getCategories();
          this.showFailureMessage('Category cannot be its own Parent');
          this.display = false;
        } else {
          this.categoryService.updateCategory(this.category).subscribe((data: any) => {
            this.getCategories();
            this.showSuccessMessage();
            this.display = false;
          });
        }
      }
  }

  public deleteCategory(cat: Category): void {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to Delete this Category?',
      header: 'Delete Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.categoryService.deleteCategory(cat).subscribe((data: any) => {
          this.getCategories();
        });
        this.msgs = [{ severity: 'success', summary: 'Confirmed', detail: 'You have accepted' }];
      },
      reject: () => {
        this.msgs = [{ severity: 'error', summary: 'Rejected', detail: 'You have rejected' }];
      }
    });
  }

  public showSuccessMessage(): void {
    this.msgs = [];
    this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Category Saved' });
  }

  public showFailureMessage(failmsg: string): void {
    this.msgs = [];
    this.msgs.push({ severity: 'failure', summary: 'Failure Message', detail: failmsg });
  }

  clearMessages() {
    this.messageService.clear();
  }

  public onBeforeDialogHide(): void {
    this.display = false;
  }
}
