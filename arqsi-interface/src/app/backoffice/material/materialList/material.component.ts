import { Finish } from '../../../shared/models/finish.model';
import { FinishesService } from '../../../shared/services/finishes.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Material } from '../../../shared/models/material.model';
import { Component, OnInit } from '@angular/core';
import { MaterialService } from 'src/app/shared/services/material.service';
import { Message, ConfirmationService, MessageService } from 'primeng/api';

@Component({
  selector: 'app-material',
  templateUrl: './material.component.html',
  styleUrls: ['./material.component.css']
})
export class MaterialComponent implements OnInit {
  public materials: Material[];

  public material: Material;

  public finishes = new Array<Finish>();

  public finishArray: Finish[];
  public finishesToSave = new Array<number>();

  public display = false;
  public header: string;

  msgs: Message[] = [];

  public materialForm = new FormGroup({
    id: new FormControl(''),
    name: new FormControl(''),
    description: new FormControl(''),
    finishes: new FormControl()
  });

  constructor(
    private materialService: MaterialService,
    private finishesService: FinishesService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    this.getMaterials();
    this.getfinishes();
  }

  showDialog() {
    this.display = true;
  }

  public getfinishes(): void {
    this.finishesService.getFinishes().subscribe((data: any) => {
      this.finishes = data;
    });
  }

  public getMaterials(): void {
    this.materialService.getMaterials().subscribe((data: any) => {
      this.materials = data;
    });
  }

  // get finishes by id
  public getFinishesMaterial(mfinishes: number[]): string[] {
    const finishesToDisplay = new Array<string>();
    this.finishes.forEach( fin => {
      if (mfinishes.includes(fin.id)) {
        finishesToDisplay.push(fin.name + ' ');
      }
    });
    return finishesToDisplay;
  }

  public newMaterial(): void {
    this.header = 'New Material';
    this.display = true;
    this.materialForm.get('name').setValue('');
    this.materialForm.get('description').setValue('');
    this.materialForm.get('finishes').setValue('');
  }

  public editMaterial(mat: Material): void {
    this.header = 'Edit Material';
    this.display = true;
    this.materialForm.get('id').setValue(mat.id);
    this.materialForm.get('name').setValue(mat.name);
    this.materialForm.get('description').setValue(mat.description);
    this.materialForm.get('finishes').setValue(mat.finishes);
  }

  public saveMaterial(): void {

    this.finishesToSave = new Array<number>();

    this.finishArray = this.materialForm.get('finishes').value;

    this.finishArray.forEach(element => {
      if (element.id != null) {
      this.finishesToSave.push(element.id);
      }
    });
    this.material = new Material(
      this.materialForm.get('id').value,
      this.materialForm.get('name').value,
      this.materialForm.get('description').value,
      this.finishesToSave
    );

    if (this.header === 'New Material') {    this.materialService.newMaterial(this.material).subscribe((data: any) => {
      this.getMaterials();
      this.showSuccessMessage();
      this.display = false;
    });
    }

    if (this.header === 'Edit Material') {    this.materialService.updateMaterial(this.material).subscribe((data: any) => {
      this.getMaterials();
      this.showSuccessMessage();
      this.display = false;
    });
    }

  }

  public deleteMaterial(mat: Material): void {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to Delete this Material?',
      header: 'Delete Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.materialService.deleteMaterial(mat).subscribe((data: any) => {
          this.getMaterials();
        });
        this.msgs = [{ severity: 'success', summary: 'Confirmed', detail: 'You have accepted' }];
      },
      reject: () => {
        this.msgs = [{ severity: 'error', summary: 'Rejected', detail: 'You have rejected' }];
      }
    });
  }

  public showSuccessMessage(): void {
    this.msgs = [];
    this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Material Saved' });
  }

  clearMessages() {
    this.messageService.clear();
  }

  public onBeforeDialogHide(): void {
    this.display = false;
  }
}
