import { Product } from '../../shared/models/product.model';
import { ProductService} from '../../shared/services/product.service';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Message, ConfirmationService, MessageService } from 'primeng/api';
import {Catalog} from '../../shared/models/catalog.model';
import {Material} from '../../shared/models/material.model';
import {CatalogService} from '../../shared/services/catalog.service';
import {MaterialService} from '../../shared/services/material.service';
import {FormControl, FormGroup} from '@angular/forms';
import {Dimensions} from '../../shared/models/dimensions.model';
import {DimensionsService} from '../../shared/services/dimensions.service';
import {Category} from '../../shared/models/category.model';
import {CategoryService} from '../../shared/services/category.service';
import {Restriction} from '../../shared/models/restriction.model';
import { ProductParents } from '../../shared/models/productparents.model';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  providers: [MessageService]
})
export class ProductComponent implements OnInit {

  products: Product[];
  product: Product;
  parts = new Array<Product>();
  part: Product;
  catalogs = new Array<Catalog>();
  catalog: Catalog;
  materials = new Array <Material>();
  material: Material;

  dimensions = new Array<Dimensions>();
  dimension: Dimensions = new Dimensions();

  nestedRestrictionsToSave = new Array<Restriction>();
  checkedIsRequired = false;
  checkedUseBaseMaterial = false;
  minDimensions = 0;
  maxDimensions = 0;

  okflag = false;

  categories = new Array<Category>();
  category: Category;

  partsArray: Product[];
  partsToSave = new Array<number>();

  catalogsArray: Catalog[];
  catalogsToSave = new Array<number>();

  materialsArray: Material[];
  materialsToSave = new Array<number>();

  dimensionsArray: Dimensions[];
  dimensionsToSave = new Array<number>();

  parents: ProductParents;

  public display = false;
  public header: string;
  msgs: Message[] = [];
  msgsdims: Message[] = [];
  msgsrest: Message[] = [];

  public productForm = new FormGroup({
    id: new FormControl(''),
    name: new FormControl(''),
    description: new FormControl(''),
    reference: new FormControl(''),
    categoryId: new FormControl(null),
    partsIds: new FormControl(),
    catalogs: new FormControl(),
    materials: new FormControl(),
    dimensions: new FormControl(),
    nestedRestrictions: new FormControl()
  });

  constructor(
    private productService: ProductService,
    private materialService: MaterialService,
    private catalogService: CatalogService,
    private dimensionsService: DimensionsService,
    private categoryService: CategoryService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
  ) { }

  addSingle() {
    this.messageService.add({severity: 'success', summary: 'Service Message', detail: 'Via MessageService'});
  }

  addMultiple() {
    this.messageService.addAll([{severity: 'success', summary: 'Service Message', detail: 'Via MessageService'},
      {severity: 'info', summary: 'Info Message', detail: 'Via MessageService'}]);
  }

  clear() {
    this.messageService.clear();
  }

  ngOnInit() {
    this.getProducts();
    this.getMaterials();
    this.getCatalogs();
    this.getDimensions();
    this.getCategories();
  }

  public getProducts(): void {
    this.productService.getProduct().subscribe((data: any) => {
      this.products = data;
      this.parts = data;
    });
  }

  public getMaterials(): void {
    this.materialService.getMaterials().subscribe((data: any) => {
      this.materials = data;
    });
  }

  public getCatalogs(): void {
    this.catalogService.getCatalogs().subscribe((data: any) => {
      this.catalogs = data;
    });
  }

  public getDimensions(): void {
    this.dimensionsService.getDimensions().subscribe((data: any) => {
      this.dimensions = data;
    });
  }

  public getCategories(): void {
    this.categoryService.getCategory().subscribe((data: any) => {
      this.categories = data;
    });
  }

  // get category by id
  public getCategoryById(idcategory: number): string {
    let categoryToDisplay = 'No Category';
    this.categories.forEach((cat) => {
      if (cat.id === idcategory) {
        categoryToDisplay = cat.name;
      }
    });
    return categoryToDisplay;
  }

  // get parts by id
  public getPartsById(partids: number[]): string[] {
    const partsToDisplay = new Array<string>();
    this.parts.forEach( part => {
      if (partids.includes(part.productId)) {
        partsToDisplay.push(part.name + ' ');
      }
    });
    return partsToDisplay;
  }

  // get catalogs by id
  public getCatalogsById(catids: number[]): string[] {
    const catalogsToDisplay = new Array<string>();
    this.catalogs.forEach( cat => {
      if (catids.includes(cat.id)) {
        catalogsToDisplay.push(cat.name + ' ');
      }
    });
    return catalogsToDisplay;
  }

  // get materials by id
  public getMaterialsById(matids: number[]): string[] {
    const materialsToDisplay = new Array<string>();
    this.materials.forEach( mat => {
      if (matids.includes(mat.id)) {
        materialsToDisplay.push(mat.name + ' ');
      }
    });
    return materialsToDisplay;
  }

  // get dimensions by id
  public getDimensionsById(dimids: number[]): string[] {
    const dimensionsToDisplay = new Array<string>();
    this.dimensions.forEach( dim => {
      if (dimids.includes(dim.id)) {
        dimensionsToDisplay.push('from: ' + dim.lengthMin + 'x' + dim.widthMin + 'x' + dim.heightMin + ' to: ' + dim.lengthMax + 'x' + dim.widthMax + 'x' + dim.heightMax + ' ');
      }
    });
    return dimensionsToDisplay;
  }

  // get one dimensions by id
  public getOneDimensionsById(iddimensions: number): string {
    let dimensionsToDisplay = 'No Dimensions';
    this.dimensions.forEach((dim) => {
      if (dim.id === iddimensions) {
        dimensionsToDisplay = ('from: ' + dim.lengthMin + 'x' + dim.widthMin + 'x' + dim.heightMin + ' to: ' + dim.lengthMax + 'x' + dim.widthMax + 'x' + dim.heightMax + ' ');
      }
    });
    return dimensionsToDisplay;
  }

  // get category by id
  public showRestriction(restriction: Restriction): string {
    return 'Min %:' + restriction.MinDimensions + ' Max %:' + restriction.MaxDimensions + ' Required:' + restriction.IsRequired + ' BaseMaterial:' + restriction.UseBaseProductMaterial;
  }

  // get parents by id
  public getParents(prod: Product): void {
    this.parents = new ProductParents(0, ' ', []);
    this.productService.getParents(prod).subscribe((data: any) => {
      this.parents = data;
    });
  }

  public newProduct(): void {
    this.header = 'New Product';
    this.display = true;
    this.productForm.get('name').setValue('');
    this.productForm.get('description').setValue('');
    this.productForm.get('reference').setValue('');
    this.productForm.get('categoryId').setValue(null);
    this.productForm.get('partsIds').setValue(null);
    this.productForm.get('catalogs').setValue(null);
    this.productForm.get('materials').setValue(null);
    this.productForm.get('dimensions').setValue(null);
    this.productForm.get('nestedRestrictions').setValue(null);
  }

  public editProduct(prod: Product): void {
    this.header = 'Edit Product';
    this.display = true;
    this.productForm.get('id').setValue(prod.productId);
    this.productForm.get('name').setValue(prod.name);
    this.productForm.get('description').setValue(prod.description);
    this.productForm.get('reference').setValue(prod.reference);
    this.productForm.get('categoryId').setValue(prod.categoryId);
    this.productForm.get('partsIds').setValue(prod.partsIds);
    this.productForm.get('catalogs').setValue(prod.catalogs);
    this.productForm.get('materials').setValue(prod.materials);
    this.productForm.get('dimensions').setValue(prod.dimensionses);
    this.productForm.get('nestedRestrictions').setValue(prod.nestedRestrictions);
  }

  public saveProduct(): void {

    this.partsArray = this.productForm.get('partsIds').value;

    if (this.partsArray != null) {
      this.partsArray.forEach(element2 => {
        if (element2.productId != null) {
          this.partsToSave.push(element2.productId);
        }
      });
    } else {
      this.partsToSave = new Array<number>();
    }

    this.catalogsArray = this.productForm.get('catalogs').value;

    if (this.catalogsArray !== null) {
      this.catalogsArray.forEach(element2 => {
        if (element2.id != null) {
          this.catalogsToSave.push(element2.id);
        }
      });
    } else {
      this.catalogsToSave = new Array<number>();
    }

    this.materialsArray = this.productForm.get('materials').value;

    this.materialsArray.forEach(element2 => {
      if (element2.id != null) {
        this.materialsToSave.push(element2.id);
      }
    });

    /*this.dimensionsArray = this.productForm.get('dimensions').value;

    this.dimensionsArray.forEach(element2 => {
      if (element2.id != null) {
        this.dimensionsToSave.push(element2.id);
      }
    });*/
    // ensuring resitrctions are specified for all parts if the number of restrictions is incomplete
    if (this.nestedRestrictionsToSave.length > 0 ) {
      let count = this.partsToSave.length - this.nestedRestrictionsToSave.length;

      while (count > 0) {
        this.nestedRestrictionsToSave.push(new Restriction(0, 0, 0, 0));
        count--;
      }
    }

    // if we have dimensions
    if (this.dimensionsToSave.length > 0) {
      this.product = new Product(
      this.productForm.get('id').value,
      this.productForm.get('name').value,
      this.productForm.get('description').value,
      this.productForm.get('reference').value,
      this.productForm.get('categoryId').value.id,
      this.partsToSave,
      this.catalogsToSave,
      this.materialsToSave,
      this.dimensionsToSave,
      this.nestedRestrictionsToSave
      );

      // console.log(this.product);

      // this.showSuccessMessage();
      this.display = false;

      if (this.header === 'New Product') {
          this.productService.newProduct(this.product).subscribe((data: any) => {
            this.showSuccessMessage();
            this.display = false;
            this.getProducts();
            location.reload();
        });
      }

      if (this.header === 'Edit Product') {
        this.productService.updateProduct(this.product).subscribe((data: any) => {
            this.showSuccessMessage();
            this.display = false;
            this.getProducts();
            location.reload();
        });
      }

      this.getProducts();
      location.reload();
    } else {
      this.showFailureMessage('No dimensions were added');
    }
  }

  public deleteProduct(prod: Product): void {
    let ispart = false;
    this.products.forEach((product) => {
     if (product.partsIds.includes(prod.productId)) {
       ispart = true;
     }
   });

    // if the current product is not a part of another product
    if (!ispart) {

      this.confirmationService.confirm({
        message: 'Are you sure that you want to Delete this Product?',
        header: 'Delete Confirmation',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
          this.productService.deleteProduct(prod).subscribe((data: any) => {
            this.getProducts();
          });
          this.msgs = [{severity: 'success', summary: 'Confirmed', detail: 'You have accepted'}];
        },
        reject: () => {
          this.msgs = [{severity: 'error', summary: 'Rejected', detail: 'You have rejected'}];
        }
      });
    } else {
      this.msgs = [{severity: 'error', summary: 'Refused', detail: 'This Product is currently part of another one'}];
    }
  }

  showSuccessDim() {
    this.msgsdims.push({severity: 'success', summary: 'Success Message', detail: 'Dimension submitted'});
  }

  showSuccessRest() {
    this.msgsrest.push({severity: 'success', summary: 'Success Message', detail: 'Restriction submitted'});
  }

  public showSuccessMessage(): void {
    this.msgs = [];
    this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Product Saved' });
  }

  public showFailureMessage(failmsg: string): void {
    this.msgs = [];
    this.msgs.push({ severity: 'failure', summary: 'Failure Message', detail: failmsg });
  }

  clearMessages() {
    this.messageService.clear();
  }

  public async newDimension(dimension: Dimensions) {
    await this.dimensionsService.newDimensions(dimension).then(
       (data: any) => {
        this.display = true;
         this.dimensionsToSave.push(data.id);
        // this.showSuccessDim();
         this.getDimensions();
      },
    error => {
      console.log(error);
      }
      );
    }

    public newRestriction(): void {
      let partsSelected;
      partsSelected = this.productForm.get('partsIds').value;

      if ( partsSelected === null) {
        partsSelected = new Array<number>();
      }

      if ( partsSelected.length > 0 && this.nestedRestrictionsToSave.length < partsSelected.length ) {
         const rest = new Restriction(
           0,
           0,
           0,
           0
         );
         rest.IsRequired = + this.checkedIsRequired;
         rest.UseBaseProductMaterial = + this.checkedUseBaseMaterial;
         rest.MinDimensions = this.minDimensions;
         rest.MaxDimensions = this.maxDimensions;
         this.nestedRestrictionsToSave.push(rest);
      }
    }

      public removeRestriction(): void {
        this.nestedRestrictionsToSave.pop();
    }

    /*
    public testRestriction(): void {
      console.log(this.nestedRestrictionsToSave);
    }*/

    public removeDimension() {
      if (this.dimensionsToSave.length > 0) {
        const dimsToDel = new Dimensions();
        dimsToDel.id = this.dimensionsToSave.pop();
        this.dimensionsService.deleteDimensions(dimsToDel).subscribe((data: any) => {
          this.getDimensions();
        });
      }
    }

    public onBeforeDialogHide(): void {
      while ( this.dimensionsToSave.length > 0 ) {
        this.removeDimension();
      }
      this.display = false;
    }

}
