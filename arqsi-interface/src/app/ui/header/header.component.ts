import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public items_manager: MenuItem[];
  public items_order: MenuItem[];

  constructor() { }

  ngOnInit() {
    this.items_manager = [
      {
        label: 'Catalog Manager', icon: 'pi pi-folder-open',
        items: [
          [
            {
              // label: 'TV 1',
              items: [
                {label: 'Material', routerLink: '/backoffice/material'},
                {label: 'Finish', routerLink: '/backoffice/finish'},
                {label: 'Catalog', routerLink: '/backoffice/catalog'},
                {label: 'Category', routerLink: '/backoffice/category'},
                {label: 'Product', routerLink: '/backoffice/product'}
                ]
            }
          ]
        ]
      },
      {
        label: 'Orders', icon: 'pi pi-list',
        items: [
          [
            {
              // label: 'Sports 1',
              items: [
                {label: 'Create a new order', routerLink: '/encomendas'},
                {label: 'View all orders', routerLink: '/orders'}
                ]
            }

          ]
        ]
      }
    ];

    /*
    this.items_order = [
      {
        label: 'Orders',
        items: [
          [
            {
              // label: 'Sports 1',
              items: [
                {label: 'Create a new order', routerLink: '/encomendas'},
                {label: 'View all orders', routerLink: '/orders'}
              ]
            }

          ]
        ]
      }
    ];*/
  }

}
