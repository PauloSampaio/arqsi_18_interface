import {Item} from './item.model';

export class Order {
  constructor(
    public _id?: string,
    public items = new Array<Item>(),
    public createdAt?: Date
  ) {}
}
