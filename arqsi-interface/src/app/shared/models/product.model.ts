import {Restriction} from './restriction.model';

export class Product {
  constructor(
    public productId?: number,
    public name: string = '',
    public description: string = '',
    public reference: string = '',
    public categoryId: number = null,
    public partsIds: number[] = null,
    public catalogs: number[] = null,
    public materials: number[] = null,
    public dimensionses: number[] = null,
    public nestedRestrictions: Restriction[] = null,
  ) {}
}
