export class Catalog {
  constructor (
    public id?: number,
    public name: string = '',
    public description: string = '',
    public date: string = '',
    public products: number[] = null
  ) {}
}
