export class ProductParents {
  constructor(
    public productId?: number,
    public name: string = '',
    public parentProducts: number[] = null,
  ) {}
}
