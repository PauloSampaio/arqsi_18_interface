export class Dimensions {
  constructor(
    public id?: number,
    public lengthMin: number = null,
    public lengthMax: number = null,
    public widthMin: number = null,
    public widthMax: number = null,
    public heightMin: number = null,
    public heightMax: number = null
  ) {}

  // caber
  public checkFitsIn(dimensions: Dimensions): boolean {
        return this.lengthMin < dimensions.lengthMin && this.lengthMax < dimensions.lengthMax &&
          this.widthMin < dimensions.widthMin && this.widthMax < dimensions.widthMax &&
          this.heightMin < dimensions.heightMin && this.heightMax < dimensions.heightMax;
  }
}
