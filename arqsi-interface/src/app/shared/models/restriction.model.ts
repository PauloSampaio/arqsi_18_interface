export class Restriction {
  constructor(
    public IsRequired: number = 0,
    public UseBaseProductMaterial: number = 0,
    public MinDimensions: number = 0,
    public MaxDimensions: number = 0,
  ) {}
}
