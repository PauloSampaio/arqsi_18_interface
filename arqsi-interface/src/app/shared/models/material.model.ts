export class Material {
  constructor(
    public id?: number,
    public name: string = null,
    public description: string = null,
    public finishes: number[] = null
  ) {}
}
