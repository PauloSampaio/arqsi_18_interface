export class Item {
  constructor(
    public _id?: number,
    public ProductId: number = null,
    public MaterialId: number = null,
    public FinishId: number = null,
    public Length: number = null,
    public Width: number = null,
    public Height: number = null,
    public ParentId: number = null,
    public order?: string
  ) {}
}
