export class Finish {
  constructor(public id?: number, public name: string = null, public description: string = null) {}
}
