export class Category {
  constructor(
    public id?: number,
    public name: string = '',
    public description: string = '',
    public parentId: number = null,
    public children: number[] = null
  ) {}
}
