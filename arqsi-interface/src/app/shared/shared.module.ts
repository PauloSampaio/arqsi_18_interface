import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { PanelModule } from 'primeng/panel';
import { ButtonModule } from 'primeng/button';
import { SliderModule } from 'primeng/slider';
import { TableModule } from 'primeng/table';
import { ListboxModule } from 'primeng/listbox';
import { CardModule } from 'primeng/card';
import {ToastModule} from 'primeng/toast';
import {ToggleButtonModule} from 'primeng/togglebutton';
import {RadioButtonModule} from 'primeng/radiobutton';
import {CheckboxModule} from 'primeng/checkbox';

import { MenuModule } from 'primeng/menu';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from 'primeng/inputtext';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { MessageModule } from 'primeng/message';
import { MessagesModule } from 'primeng/messages';

import {ScrollPanelModule} from 'primeng/scrollpanel';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    PanelModule,
    ButtonModule,
    SliderModule,
    TableModule,
    ListboxModule,
    CardModule,
    MenuModule,
    DialogModule,
    InputTextModule,
    ConfirmDialogModule,
    MessageModule,
    MessagesModule,
    ToastModule,
    ToggleButtonModule,
    RadioButtonModule,
    CheckboxModule,
    ScrollPanelModule
  ],
  exports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    PanelModule,
    ButtonModule,
    SliderModule,
    TableModule,
    ListboxModule,
    CardModule,
    MenuModule,
    DialogModule,
    InputTextModule,
    ConfirmDialogModule,
    MessageModule,
    MessagesModule,
    ToastModule,
    ToggleButtonModule,
    RadioButtonModule,
    CheckboxModule,
    ScrollPanelModule
  ],
  declarations: [],
  providers: []
})
export class SharedModule {}
