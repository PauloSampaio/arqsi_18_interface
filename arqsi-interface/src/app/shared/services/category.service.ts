import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import * as Constants from './../app-constants';
import {Category} from '../models/category.model';

@Injectable()
export class CategoryService {
  private endpoint = 'category';

  constructor(private httpService: HttpClient) {
  }

  public newCategory(category: Category): Observable<Category> {
    return this.httpService
      .post(`${Constants.APP_URI}/${this.endpoint}`, category)
      .pipe(map((response: Category) => response));
  }

  public getCategory(): Observable<[Category]> {
    return this.httpService
      .get(`${Constants.APP_URI}/${this.endpoint}`)
      .pipe(map((response: [Category]) => {
          return response;
        })
      );
  }

  public updateCategory(category: Category): Observable<Category> {
    let headers = new HttpHeaders();
    headers.append('Content-type', 'application/json');
    return this.httpService
      .put<Category>(`${Constants.APP_URI}/${this.endpoint}/${category.id}`, category, {
        headers: headers
      })
      .pipe(map((response: Category) => response));
  }

  public deleteCategory(category: Category): Observable<Category> {
    return this.httpService
      .delete<Category>(`${Constants.APP_URI}/${this.endpoint}/${category.id}`)
      .pipe(map((response: any) => response));
  }
}
