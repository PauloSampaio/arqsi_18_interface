import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as Constants from './../app-constants';
import { Material } from '../models/material.model';

@Injectable()
export class MaterialService {
  private endpoint = 'material';

  constructor(private httpService: HttpClient) {}

  public newMaterial(material: Material): Observable<Material> {
    return this.httpService
      .post(`${Constants.APP_URI}/${this.endpoint}`, material)
      .pipe(map((response: Material) => response));
  }

  public getMaterials(): Observable<[Material]> {
    return this.httpService.get(`${Constants.APP_URI}/${this.endpoint}`).pipe(
      map((response: [Material]) => {
        return response;
      })
    );
  }

  public updateMaterial(material: Material): Observable<Material> {
    let headers = new HttpHeaders();
    headers.append('Content-type', 'application/json');
    return this.httpService
      .put<Material>(`${Constants.APP_URI}/${this.endpoint}/${material.id}`, material, {
        headers: headers
      })
      .pipe(map((response: Material) => response));
  }

  public deleteMaterial(material: Material): Observable<Material> {
    return this.httpService
      .delete<Material>(`${Constants.APP_URI}/${this.endpoint}/${material.id}`)
      .pipe(map((response: any) => response));
  }
}
