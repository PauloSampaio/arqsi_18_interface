import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Item } from '../models/item.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as Constants from './../app-constants';
import {Catalog} from '../models/catalog.model';
import {Product} from '../models/product.model';
import {Material} from '../models/material.model';
import {Category} from '../models/category.model';
import {Finish} from '../models/finish.model';
import {Dimensions} from '../models/dimensions.model';
import {Order} from '../models/order.model';

@Injectable()
export class OrdersService {
  private endpoint = 'orders';
  private catalogEndpoint = 'catalogs';
  private materialEndpoint = 'material';
  private productEndpoint = 'product';
  private categoryEndpoint = 'category';
  private finishEndpoint = 'finish';
  private dimensionEndpoint = 'dimension';
  private itemsEndpoint = 'items';

  constructor(private httpService: HttpClient) {}

  public getCatalogs(): Observable<[Catalog]> {
    return this.httpService.get(`${Constants.APP_URL}/${this.catalogEndpoint}`).pipe(
      map((response: [Catalog]) => {
        return response;
      })
    );
  }

  public getProductsCatalog(catalogId: number): Observable<[Product]> {
    const headers = new HttpHeaders();
    headers.append('Content-type', 'application/json');
    return this.httpService
      .get(`${Constants.APP_URL}/${this.catalogEndpoint}/${catalogId}`).pipe(
        map((response: [Product]) => {
          return response;
        })
      );
  }

  public getMaterials(): Observable<[Material]> {
    const headers = new HttpHeaders();
    headers.append('Content-type', 'application/json');
    return this.httpService
      .get(`${Constants.APP_URL}/${this.materialEndpoint}`).pipe(
        map((response: [Material]) => {
          return response;
        })
      );
  }

  public getProducts(): Observable<[Product]> {
    const headers = new HttpHeaders();
    headers.append('Content-type', 'application/json');
    return this.httpService
      .get(`${Constants.APP_URL}/${this.productEndpoint}`).pipe(
        map((response: [Product]) => {
          return response;
        })
      );
  }

  public getCategories(): Observable<[Category]> {
    const headers = new HttpHeaders();
    headers.append('Content-type', 'application/json');
    return this.httpService
      .get(`${Constants.APP_URL}/${this.categoryEndpoint}`).pipe(
        map((response: [Category]) => {
          return response;
        })
      );
  }

  public getFinishes(): Observable<[Finish]> {
    const headers = new HttpHeaders();
    headers.append('Content-type', 'application/json');
    return this.httpService
      .get(`${Constants.APP_URL}/${this.finishEndpoint}`).pipe(
        map((response: [Finish]) => {
          return response;
        })
      );
  }

  public getDimensions(): Observable<[Dimensions]> {
    const headers = new HttpHeaders();
    headers.append('Content-type', 'application/json');
    return this.httpService
      .get(`${Constants.APP_URL}/${this.dimensionEndpoint}`).pipe(
        map((response: [Dimensions]) => {
          return response;
        })
      );
  }

  public newOrder(order: Order): Observable<Order> {
    console.log('ORDER2: ', order);
    return this.httpService
      .post(`${Constants.APP_URL}/${this.endpoint}`, order)
      .pipe(map((response: Order) => response));
  }

  public getOrders(): Observable<[Order]> {
    return this.httpService.get(`${Constants.APP_URL}/${this.endpoint}`).pipe(
      map((response: [Order]) => {
        return response;
      })
    );
  }

  public updateOrder(order: Order): Observable<Order> {
    const headers = new HttpHeaders();
    headers.append('Content-type', 'application/json');
    return this.httpService
      .put<Order>(`${Constants.APP_URL}/${this.endpoint}/${order._id}`, order, {
        headers: headers
      })
      .pipe(map((response: Order) => response));
  }

  public deleteOrder(order: Order): Observable<Order> {
    return this.httpService
      .delete<Order>(`${Constants.APP_URL}/${this.endpoint}/${order._id}`)
      .pipe(map((response: any) => response));
  }

  public getItems(): Observable<[Item]> {
    return this.httpService
      .get(`${Constants.APP_URL}/${this.itemsEndpoint}`).pipe(
        map((response: [Item]) => {
          return response;
        })
      );
  }

  public deleteItem(item: Item): Observable<Item> {
    return this.httpService
      .delete<Item>(`${Constants.APP_URL}/${this.endpoint}/${item.order}/${this.itemsEndpoint}/${item._id}`)
      .pipe(map((response: any) => response));
  }
}
