import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as Constants from './../app-constants';
import { Finish } from '../models/finish.model';

@Injectable()
export class FinishesService {
  private endpoint = 'finish';

  constructor(private httpService: HttpClient) {}

  public newFinishes(finish: Finish): Observable<Finish> {
    return this.httpService
      .post(`${Constants.APP_URI}/${this.endpoint}`, finish)
      .pipe(map((response: Finish) => response));
  }

  public getFinishes(): Observable<[Finish]> {
    return this.httpService.get(`${Constants.APP_URI}/${this.endpoint}`).pipe(
      map((response: [Finish]) => {
        return response;
      })
    );
  }

  public updateFinish(finish: Finish): Observable<Finish> {
    let headers = new HttpHeaders();
    headers.append('Content-type', 'application/json');
    return this.httpService
      .put<Finish>(`${Constants.APP_URI}/${this.endpoint}/${finish.id}`, finish, {
      headers: headers
    })
      .pipe(map((response: Finish) => response));
  }

  public deleteFinish(finish: Finish): Observable<Finish> {
    return this.httpService
      .delete<Finish>(`${Constants.APP_URI}/${this.endpoint}/${finish.id}`)
      .pipe(map((response: any) => response));
  }
}
