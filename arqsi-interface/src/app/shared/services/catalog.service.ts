import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import * as Constants from './../app-constants';
import {Catalog} from '../models/catalog.model';

@Injectable()
export class CatalogService {
  private endpoint = 'catalog';

  constructor(private httpService: HttpClient) {
  }

  public newCatalog(catalog: Catalog): Observable<Catalog> {
    return this.httpService
      .post(`${Constants.APP_URI}/${this.endpoint}`, catalog)
      .pipe(map((response: Catalog) => response));
  }

  public getCatalogs(): Observable<[Catalog]> {
    return this.httpService
      .get(`${Constants.APP_URI}/${this.endpoint}`)
      .pipe(map((response: [Catalog]) => {
          return response;
        })
      );
  }

  public updateCatalog(catalog: Catalog): Observable<Catalog> {
    let headers = new HttpHeaders();
    headers.append('Content-type', 'application(json');
    return this.httpService
      .put<Catalog>(`${Constants.APP_URI}/${this.endpoint}/${catalog.id}`, catalog, {
        headers: headers
      })
      .pipe(map((response: Catalog) => response));
  }

  public deleteCatalog(catalog: Catalog): Observable<Catalog> {
    return this.httpService
      .delete<Catalog>(`${Constants.APP_URI}/${this.endpoint}/${catalog.id}`)
      .pipe(map((response: any) => response));
  }
}
