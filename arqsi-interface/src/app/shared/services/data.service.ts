import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class DataService {

  private idSource = new BehaviorSubject(null);
  currentId = this.idSource.asObservable();

  constructor() { }

  changeId(id: number) {
    this.idSource.next(id);
  }

}
