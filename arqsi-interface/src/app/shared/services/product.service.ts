import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as Constants from './../app-constants';
import {Product} from '../models/product.model';
import {ProductParents} from '../models/productparents.model';

@Injectable()
export class ProductService {
  private endpoint = 'product';

  constructor(private httpService: HttpClient) {}

  public newProduct(product: Product): Observable<Product> {
    return this.httpService
      .post(`${Constants.APP_URI}/${this.endpoint}`, product)
      .pipe(map((response: Product) => response));
  }

  public getProduct(): Observable<[Product]> {
    return this.httpService.get(`${Constants.APP_URI}/${this.endpoint}`).pipe(
      map((response: [Product]) => {
        return response;
      })
    );
  }

  public getParents(product: Product): Observable<ProductParents> {
    return this.httpService.get(`${Constants.APP_URI}/${this.endpoint}/${product.productId}/${'partof'}`).pipe(
      map((response: ProductParents) => {
        return response;
      })
    );
  }

  public updateProduct(product: Product): Observable<Product> {
    let headers = new HttpHeaders();
    headers.append('Content-type', 'application/json');
    return this.httpService.put<Product>(`${Constants.APP_URI}/${this.endpoint}/${product.productId}`, product, {
      headers: headers
    }).pipe(map((response: Product) => response));
  }

  public deleteProduct(product: Product): Observable<Product> {
    return this.httpService.delete<Product>(`${Constants.APP_URI}/${this.endpoint}/${product.productId}`)
      .pipe(map((response: any) => response));
  }
}
