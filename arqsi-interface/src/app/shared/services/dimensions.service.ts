import { Injectable } from '@angular/core';
import { Dimensions } from '../models/dimensions.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as Constants from './../app-constants';
import {Category} from '../models/category.model';

@Injectable()
export class DimensionsService {
  private endpoint = 'dimensions';
  constructor(private httpService: HttpClient) {}

  public async newDimensions(dimensions: Dimensions) {
    return await this.httpService
      .post(`${Constants.APP_URI}/${this.endpoint}`, dimensions)
      .pipe(map((response: Dimensions) => response)).toPromise();
  }

  public deleteDimensions(dimensions: Dimensions): Observable<Dimensions> {
    return this.httpService
      .delete<Dimensions>(`${Constants.APP_URI}/${this.endpoint}/${dimensions.id}`)
      .pipe(map((response: any) => response));
  }

  public getDimensions(): Observable<[Dimensions]> {
    return this.httpService.get(`${Constants.APP_URI}/${this.endpoint}`).pipe(
      map((response: [Dimensions]) => {
        return response;
      })
    );
  }
}
