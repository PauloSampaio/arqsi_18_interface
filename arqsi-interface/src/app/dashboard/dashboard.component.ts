import { MenuItem } from 'primeng/api';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  constructor(private router: Router) {}

  private gestorItems: MenuItem[];

  private utilizadorItems: MenuItem[];

  ngOnInit() {
    this.gestorItems = [
      {
        label: 'Catalog Manager',
        items: [
          { label: 'Material', icon: 'fa fa-plus', routerLink: '/backoffice/material' },
          { label: 'Finish', icon: 'fa fa-plus', routerLink: '/backoffice/finish' },
          { label: 'Catalog', icon: 'fa fa-plus', routerLink: '/backoffice/catalog' },
          { label: 'Category', icon: 'fa fa-plus', routerLink: '/backoffice/category' },
          { label: 'Produto', icon: 'fa fa-plus', routerLink: '/backoffice/product' }
        ]
      }
    ];

    this.utilizadorItems = [
      {
        label: 'Orders',
        items: [
          { label: 'Create New Order', icon: 'fa fa-plus', routerLink: '/encomendas' },
          { label: 'View All Orders', icon: 'fa fa-plus', routerLink: '/orders' }
        ]
      }
    ];
  }
}
