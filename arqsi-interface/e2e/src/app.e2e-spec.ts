import { DashBoardPage } from './app.po';
import { MaterialsPage } from './app.po';
import { FinishesPage } from './app.po';
import { CatalogsPage } from './app.po';
import { CategoriesPage } from './app.po';
import { ProductsPage } from './app.po';
import { NewOrdersPage } from './app.po';
import { ListOrdersPage } from './app.po';

const title = 'CustomFurniture';

// ***************
// HOME PAGE
// ***************
describe('DashBoardPage App', () => {
  let page: DashBoardPage;

  beforeEach(() => {
    page = new DashBoardPage();
  });

  it('should have title', () => {
    page.navigateTo();
    expect(page.getTitle()).toEqual(title);
  });

  it('should have welcome message', () => {
    page.navigateTo();
    expect(page.getMessage()).toEqual('WELCOME TO CUSTOM FURNITURE!');
  });

});

// ***************
// MATERIALS PAGE
// ***************
describe('MaterialsPage App', () => {
  let page: MaterialsPage;

  beforeEach(() => {
    page = new MaterialsPage();
  });

  it('should have title', () => {
    page.navigateTo();
    expect(page.getTitle()).toEqual(title);
  });

  it('should have New button', () => {
    page.navigateTo();
    expect(page.getNewButton().getText()).toContain('New');
  });

});

// ***************
// FINISHES PAGE
// ***************
describe('FinishesPage App', () => {
  let page: FinishesPage;

  beforeEach(() => {
    page = new FinishesPage();
  });

  it('should have title', () => {
    page.navigateTo();
    expect(page.getTitle()).toEqual(title);
  });

  it('should have New button', () => {
    page.navigateTo();
    expect(page.getNewButton().getText()).toContain('New');
  });

});

// ***************
// CATALOGS PAGE
// ***************
describe('CatalogsPage App', () => {
  let page: CatalogsPage;

  beforeEach(() => {
    page = new CatalogsPage();
  });

  it('should have title', () => {
    page.navigateTo();
    expect(page.getTitle()).toEqual(title);
  });

  it('should have New button', () => {
    page.navigateTo();
    expect(page.getNewButton().getText()).toContain('New');
  });

});

// ***************
// CATEGORIES PAGE
// ***************
describe('CategoriesPage App', () => {
  let page: CategoriesPage;

  beforeEach(() => {
    page = new CategoriesPage();
  });

  it('should have title', () => {
    page.navigateTo();
    expect(page.getTitle()).toEqual(title);
  });

  it('should have New button', () => {
    page.navigateTo();
    expect(page.getNewButton().getText()).toContain('New');
  });

});

// ***************
// PRODUCTS PAGE
// ***************
describe('ProductsPage App', () => {
  let page: ProductsPage;

  beforeEach(() => {
    page = new ProductsPage();
  });

  it('should have title', () => {
    page.navigateTo();
    expect(page.getTitle()).toEqual(title);
  });

  it('should have New button', () => {
    page.navigateTo();
    expect(page.getNewButton().getText()).toContain('New');
  });

});

// ***************
// NEW ORDERS PAGE
// ***************
describe('NewOrdersPage App', () => {
  let page: NewOrdersPage;

  beforeEach(() => {
    page = new NewOrdersPage();
  });

  it('should have title', () => {
    page.navigateTo();
    expect(page.getTitle()).toEqual(title);
  });

});

// ****************
// LIST ORDERS PAGE
// ****************
describe('ListOrdsPage App', () => {
  let page: ListOrdersPage;

  beforeEach(() => {
    page = new ListOrdersPage();
  });

  it('should have title', () => {
    page.navigateTo();
    expect(page.getTitle()).toEqual(title);
  });

});
