import { browser, by, element } from 'protractor';

// ***************
// HOME PAGE
// ***************
export class DashBoardPage {
  navigateTo() {
    return browser.get('http://localhost:4200/dashboard');
  }

  /*getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }*/

  getTitle() {
    return browser.getTitle();
  }

  getMessage() {
    return element(by.cssContainingText('h1', 'WELCOME')).getText();
  }
}

// ***************
// MATERIALS PAGE
// ***************
export class MaterialsPage {
  navigateTo() {
    return browser.get('http://localhost:4200/backoffice/material');
  }

  getTitle() {
    return browser.getTitle();
  }

  getNewButton() {
    return element(by.cssContainingText('button', 'New'));
  }
}

// ***************
// FINISHES PAGE
// ***************
export class FinishesPage {
  navigateTo() {
    return browser.get('http://localhost:4200/backoffice/finish');
  }

  getTitle() {
    return browser.getTitle();
  }

  getNewButton() {
    return element(by.cssContainingText('button', 'New'));
  }
}

// ***************
// CATALOGS PAGE
// ***************
export class CatalogsPage {
  navigateTo() {
    return browser.get('http://localhost:4200/backoffice/catalog');
  }

  getTitle() {
    return browser.getTitle();
  }

  getNewButton() {
    return element(by.cssContainingText('button', 'New'));
  }
}

// ***************
// CATEGORIES PAGE
// ***************
export class CategoriesPage {
  navigateTo() {
    return browser.get('http://localhost:4200/backoffice/category');
  }

  getTitle() {
    return browser.getTitle();
  }

  getNewButton() {
    return element(by.cssContainingText('button', 'New'));
  }
}

// ***************
// PRODUCTS PAGE
// ***************
export class ProductsPage {
  navigateTo() {
    return browser.get('http://localhost:4200/backoffice/product');
  }

  getTitle() {
    return browser.getTitle();
  }

  getNewButton() {
    return element(by.cssContainingText('button', 'New'));
  }
}

// ***************
// NEW ORDERS PAGE
// ***************
// "encomendas"
export class NewOrdersPage {
  navigateTo() {
    return browser.get('http://localhost:4200/backoffice/encomendas');
  }

  getTitle() {
    return browser.getTitle();
  }
}

// ****************
// LIST ORDERS PAGE
// ****************
export class ListOrdersPage {
  navigateTo() {
    return browser.get('http://localhost:4200/backoffice/orders');
  }

  getTitle() {
    return browser.getTitle();
  }

}


